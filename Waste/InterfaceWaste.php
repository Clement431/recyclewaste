<?php

namespace Waste;

interface InterfaceWaste{


    public function getWeight(): float;

    public function setWeight(float $weight);

    public function addWeight(float $weight);

    public function getCo2Incineration();

    public function getCo2Recyclage();
    
}

