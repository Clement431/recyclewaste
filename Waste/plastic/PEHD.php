<?php

namespace Waste\Plastic;

use Waste\InterfaceWaste;

class PEHD implements InterfaceWaste{

    private float $weight;
    private int $co2Incineration;
    private int $co2Recyclage;

    public function __construct($weight, $co2Incineration, $co2Recyclage)
    {
        $this->weight = $weight;
        $this->co2Incineration = $co2Incineration;
        $this->co2Recyclage = $co2Recyclage;
    }


    public function getWeight(): float
    {
        return $this->weight;
    }


    public function setWeight(float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }


    public function getCo2Incineration():int
    {
        return $this->co2Incineration;
    }

    public function getCo2Recyclage():int
    {
        return $this->co2Recyclage;
    }

    public function addWeight(float $weight)
    {
        $this->weight =  $this->weight + $weight;
    }
}
