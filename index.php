<?php

use Recycle\Composter;
use Recycle\Incinerator;
use Recycle\RecycleGlass;
use Recycle\RecycleMetals;
use Recycle\RecyclePaper;
use Recycle\RecyclePlastic;
use Recycle\SortingCenter;
use Waste\Glass;
use Waste\Metals;
use Waste\Organic;
use Waste\OtherWaste;
use Waste\Paper;
use Waste\Plastic\PC;
use Waste\Plastic\PEHD;
use Waste\Plastic\PET;
use Waste\Plastic\PVC;


include './autoloader.php';

$strJsonFileContents = file_get_contents('./data/data.json');
$data = json_decode($strJsonFileContents);

$strJsonFileContents = file_get_contents('./data/co2.json');
$co2 = json_decode($strJsonFileContents);


//init all waster with co2
$wastePC = new PC(0, $co2->plastiques->PC->incineration, $co2->plastiques->PC->recyclage);
$wastePEHD = new PEHD(0, $co2->plastiques->PEHD->incineration, $co2->plastiques->PEHD->recyclage);
$wastePET = new PET(0, $co2->plastiques->PET->incineration, $co2->plastiques->PET->recyclage);
$wastePVC = new PVC(0, $co2->plastiques->PVC->incineration, $co2->plastiques->PVC->recyclage);

$wasteGlass = new Glass(0, $co2->verre->incineration, $co2->verre->recyclage);
$wasteMetals = new Metals(0, $co2->metaux->incineration, $co2->metaux->recyclage);
$wasteOrganic = new Organic(0, $co2->organique->incineration, $co2->organique->compostage);
$wastePaper = new Paper(0, $co2->papier->incineration, $co2->papier->recyclage);
$wasetOther = new OtherWaste(0, $co2->autre->incineration, 0);


$incinerator = new Incinerator(0);
$recyclePaper = new RecyclePaper(0);
$recycleGlass = new RecycleGlass(0);
$recycleMetals = new RecycleMetals(0);
$sortingCenter = new SortingCenter(0);
$composter = new Composter(0);
$recyclePlastic = new RecyclePlastic(0);


foreach ($data->services as $key => $element) {

    switch ($element->type) {
        case 'incinerateur':
            $incinerator->addCapcity($element->capaciteLigne * $element->ligneFour);
            break;

        case 'recyclagePapier':
            $recyclePaper->addCapcity($element->capacite);
            break;

        case 'recyclageVerre':
            $recyclePaper->addCapcity($element->capacite);
            break;

        case 'recyclagePlastique':
            $recyclePlastic->addCapcity($element->capacite);
            break;

        case 'recyclageMetaux':
            $recyclePaper->addCapcity($element->capacite);
            break;

        case 'centreTri':
            $sortingCenter->addCapcity($element->capacite);
            break;

        case 'composteur':
            $composter->addCapcity($element->capacite * $element->foyers);
            break;
    }
}

//set the weight of waste
foreach ($data->quartiers as $key => $quartiers) {

    $wastePET->addWeight($quartiers->plastiques->PET);
    $wastePVC->addWeight($quartiers->plastiques->PVC);
    $wastePC->addWeight($quartiers->plastiques->PC);
    $wastePEHD->addWeight($quartiers->plastiques->PEHD);

    $wasteGlass->addWeight($quartiers->verre);
    $wasteMetals->addWeight($quartiers->metaux);
    $wasteOrganic->addWeight($quartiers->organique);
    $wastePaper->addWeight($quartiers->papier);
    $wasetOther->addWeight($quartiers->autre);
}

$totalCo2=0;

$totalCo2 += $recyclePaper->recyclePaper($wastePaper);
$totalCo2 += $recycleGlass->recycleGlass($wasteGlass);
$totalCo2 += $recycleMetals->recycleMetal($wasteMetals);
$totalCo2 += $composter->recycleOrganic($wasteOrganic);
$totalCo2 += $sortingCenter->recycleOther($wasetOther);

$totalCo2 += $recyclePlastic->recyclePC($wastePC);
$totalCo2 += $recyclePlastic->recyclePEHD($wastePEHD);
$totalCo2 += $recyclePlastic->recyclePET($wastePET);
$totalCo2 += $recyclePlastic->recyclePVC($wastePVC);


echo $totalCo2;