<?php

namespace Recycle;

use Waste\Metals;

class RecycleMetals extends AbstractRecycle{

    public function recycleMetal(Metals &$waste): float
    {
        return parent::recycle($waste);

    }
}