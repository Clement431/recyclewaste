<?php

namespace Recycle;

use Waste\OtherWaste;

class SortingCenter extends AbstractRecycle{

    public function recycleOther(OtherWaste &$waste): bool
    {
        return parent::recycle($waste);

    }
}

