<?php

namespace Recycle;

use Waste\InterfaceWaste;

abstract class AbstractRecycle
{

    protected int $capacity;

    public function __construct(int $capacity)
    {
        $this->capacity = $capacity;
    }

    protected function recycle(InterfaceWaste &$waste): int
    {

        $weight = $waste->getWeight();

        if ($this->checkRecycling($weight)) {
            $this->capacity = $this->capacity - $weight;
            $co2 = $waste->getCo2Recyclage() * $weight;
            $waste->setWeight(0);
            return $co2;
        }

        $waste->setWeight($weight - $this->capacity);
        $co2 = $waste->getCo2Recyclage() * $this->capacity;
        return $co2;
    }

    private function checkRecycling($weight): bool
    {

        if (($this->capacity - $weight) > 0) {
            return true;
        }

        return false;
    }


    public function getCapacity(): int
    {
        return $this->capacity;
    }


    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function  addCapcity($capacity)
    {
        $this->capacity = $this->capacity + $capacity;
    }
}
