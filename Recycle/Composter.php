<?php

namespace Recycle;

use Waste\Organic;

class Composter extends AbstractRecycle{

    public function recycleOrganic(Organic &$waste): bool
    {
        return parent::recycle($waste);

    }
}