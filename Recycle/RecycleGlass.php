<?php

namespace Recycle;

use Waste\Glass;

class RecycleGlass extends AbstractRecycle{

    public function recycleGlass(Glass &$waste): int
    {
        return parent::recycle($waste);

    }
}