<?php

namespace Recycle;

use Waste\InterfaceWaste;

class Incinerator extends AbstractRecycle{

  public function burnWaste(InterfaceWaste &$waste):bool
  {
      return parent::recycle($waste);
  }
}