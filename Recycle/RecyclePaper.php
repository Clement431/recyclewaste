<?php

namespace Recycle;

use Waste\Paper;

class RecyclePaper extends AbstractRecycle{

    public function recyclePaper(Paper &$waste): int
    {
        return parent::recycle($waste);

    }
}