<?php

namespace Recycle;

use Waste\InterfaceWaste;
use Waste\Plastic\InterfacePlastic;
use Waste\Plastic\PC;
use Waste\Plastic\PEHD;
use Waste\Plastic\PET;
use Waste\Plastic\PVC;

class RecyclePlastic extends AbstractRecycle{

    public function recyclePC(PC &$waste): bool
    {
        return parent::recycle($waste);

    }

    public function recyclePEHD(PEHD &$waste): bool
    {
        return parent::recycle($waste);

    }

    public function recyclePET(PET &$waste): bool
    {
        return parent::recycle($waste);

    }

    public function recyclePVC(PVC &$waste): bool
    {
        return parent::recycle($waste);

    }
}