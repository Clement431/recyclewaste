# L'approche initiale

J'ai tout d'abord reformulé la problématique du brief pour me permettre de cerner toutes les problématiques. Pour cela, j'ai commencé à étudier le document data.json et co2.json qui permet de rassembler toutes les informations nécessaires.

J'ai ensuite commencé à faire une list des différents entity qui peuvent en être dégagé. J'ai donc 2 parties une avec les services de recyclage et l'autre avec les déchets. Une foi que j'ai listée toute les entity, j'ai commencé à les relier pour en faire un schéma fonctionnelle 

Une première problématique est arrivée à moi, j'ai mis beaucoup de temps à faire mon schéma, car je n'arrive pas à savoir s'il est fonctionnel et le plus optimiser possible. J'ai donc décidé de commencer à programmer sans être sur de mon schéma.

# Avancement

J'ai tout d'abord commencé à créer un service de recyclage et une classe déchet pour avoir une aperçue du fonctionnement de l'application. Une fois arriver à une solution qui me semblait correcte, j'ai donc créé les différents services et déchets.

Une première problématique, c'est présenter devant moi avec les plastiques. J'ai alors commencé de faire une interface pour les regrouper. Mais cette interface m'a semblé être la mauvaise solution, car elle n'apportait rien aux classes qui l'implémentait. J'ai donc décidé de faire une méthode de recyclage par plastique.

Une fois que toutes mes classes sont créées, j'ai instancié les différentes classes dans index.php et récupérer les valeurs du json pour les mettre dans mes objets. Après cette étape, je peux donc utiliser ma méthode pour recycler. Mais je trouve que ma méthode n'est pas la plus simple et demande de faire de la répétition de code. Et le plastique ne se recycle pas de la bonne façon.